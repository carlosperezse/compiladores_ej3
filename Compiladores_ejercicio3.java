package compiladores_ejercicio3;
import java.util.Scanner;


public class Compiladores_ejercicio3 {

    
    public static void main(String[] args) {
        
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese su palabra.");
        String palabra = s.nextLine();
        boolean repeticion =false;
        
        for(int i=1;i<palabra.length();i++){
            if(palabra.charAt(i-1)==palabra.charAt(i)){
                repeticion = true; 
            }
        }
        System.out.println("Se repiten letras seguidas: "+repeticion);
    }
    
}